package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Service.LoanApprovalService;

@RestController
public class LoanApprovalController 
{
	
	@Autowired
	LoanApprovalService loanApprovalService;
	
	
	@RequestMapping(value="/post",method=RequestMethod.POST ,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HashMap> loanApproval(@RequestBody Map<String, Object> request) {
		 HashMap loan = loanApprovalService.getApprovalStatus(request);
	return ResponseEntity.status(200).body(loan);
	}
}
