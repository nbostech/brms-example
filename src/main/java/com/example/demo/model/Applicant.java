package com.example.demo.model;

import org.springframework.stereotype.Component;

@Component
public class Applicant 
{
	private String name;
	private int creditScore;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCreditScore() {
		return creditScore;
	}
	public void setCreditScore(int creditScore) {
		this.creditScore = creditScore;
	}
	
	@Override
	public String toString() {
		return "Approval [name=" + name + ", creditScore=" + creditScore + "]";
	}
	
	
}
