package com.example.demo.model;

import org.springframework.stereotype.Component;

@Component
public class Loan 
{
  private double amount;
  private boolean approved;
  private String reason;
  private double interestRate;
  private int duration;
  
public String getReason() {
	return reason;
}
public void setReason(String reason) {
	this.reason = reason;
}
public double getInterestRate() {
	return interestRate;
}
public void setInterestRate(double interestRate) {
	this.interestRate = interestRate;
}
public int getDuration() {
	return duration;
}
public void setDuration(int duration) {
	this.duration = duration;
}
public double getAmount() {
	return amount;
}
public void setAmount(double amount) {
	this.amount = amount;
}
public boolean isApproved() {
	return approved;
}
public void setApproved(boolean approved) {
	this.approved = approved;
}
@Override
public String toString() {
	return "Loan [amount=" + amount + ", approved=" + approved + "]";
}
  
  
}
