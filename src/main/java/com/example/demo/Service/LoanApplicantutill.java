package com.example.demo.Service;

import org.springframework.stereotype.Component;

import com.example.demo.model.Applicant;
import com.example.demo.model.Loan;

@Component
public class LoanApplicantutill {
	
	Loan loan;
	Applicant applicant;

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

}
