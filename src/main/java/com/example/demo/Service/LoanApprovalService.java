package com.example.demo.Service;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class LoanApprovalService {

	public HashMap getApprovalStatus(Map<String,Object> request) {
		String predictionURL = "http://127.0.0.1:8080/kie-server/services/rest/server/containers/instances/loan-application";
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization","Basic ZG1BZG1pbjpyZWRoYXRkbTEh");
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<Map<String, Object>> http = new HttpEntity(getLoanApprovalInputObject(request),headers);
		ResponseEntity<HashMap> responseEntity = null;
		RestTemplate restCall = getRestTemplate();
		System.out.println("before");
		
		responseEntity = restCall.exchange(predictionURL, HttpMethod.POST , http , HashMap.class);
		System.out.println("after");
		return responseEntity.getBody();
	}
	
	public Map<String, Object> getLoanApprovalInputObject(Map<String,Object> request){
		List<Object> list = new ArrayList<>();
		Map<String, Object> map1 = new HashMap<>();
		Map<String, Object> map2 = new HashMap<>();
		Map<String, Object> map3 = new HashMap<>();
		Map<String, Object> map4 = new HashMap<>();
		Map<String, Object> map5 = new HashMap<>();
		Map<String, Object> map6 = new HashMap<>();
		Map<String, Object> map7 = new HashMap<>();
		
		map7.put("fire-all-rules", new HashMap<>());
		map1.put("insert", map2);
		map2.put("object", map3);
		map2.put("out-identifier","applicant");
		map3.put("com.redhat.demos.dm.loan.model.Applicant", request.get("Applicant"));
		
		map4.put("insert", map5);
		map5.put("object", map6);
		map5.put("out-identifier","loan");
		map6.put("com.redhat.demos.dm.loan.model.Loan", request.get("Loan"));
		
		list.add(map1);
		list.add(map4);
		list.add(map7);
		
		Map<String, Object> finalObject=new HashMap<>();
		finalObject.put("lookup", "default-stateless-ksession");
		finalObject.put("commands",list);
		
		System.out.println(finalObject);
		return finalObject;
		
	}
	
	
	
	private RestTemplate getRestTemplate() {
	    RestTemplate restTemplate = new RestTemplate();
	    // added StringHttpMessageConverter to handle UTF-8 encoding text
	    restTemplate.getMessageConverters().add(0,
	        new StringHttpMessageConverter(Charset.forName("UTF-8")));
	    return restTemplate;
	  }
	
	

}
